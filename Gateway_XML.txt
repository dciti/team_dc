<gateway compatibility="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://schema.itrsgroup.com/GA4.1.0-170114/gateway.xsd">
    <includes>
        <priority>1</priority>
        <include>
            <priority>3</priority>
            <required>true</required>
            <location>Types.xml</location>
        </include>
        <include>
            <priority>4</priority>
            <required>true</required>
            <location>Samplers.xml</location>
        </include>
        <includeGroup name="Rules">
            <include>
                <priority>2</priority>
                <required>true</required>
                <location>Rules.xml</location>
            </include>
            <include>
                <priority>5</priority>
                <required>true</required>
                <location>DBRules.xml</location>
            </include>
            <include>
                <priority>6</priority>
                <required>true</required>
                <location>TCPLinksRules.xml</location>
            </include>
        </includeGroup>
    </includes>
    <probes>
        <probe name="ITRS">
            <hostname>localhost</hostname>
            <port>55901</port>
        </probe>
        <probe name="TestAppServer">
            <hostname>testserver.training.local</hostname>
            <port>7036</port>
        </probe>
        <probe name="ProdAppServer">
            <hostname>prodserver.training.local</hostname>
            <port>7036</port>
        </probe>
        <probe name="ELK">
            <hostname>elk.training.local</hostname>
            <port>7036</port>
        </probe>
        <probe name="uDeploy">
            <hostname>uDeploy.training.local</hostname>
            <port>7036</port>
        </probe>
        <probe name="Docker">
            <hostname>dockerreg.training.local</hostname>
            <port>7036</port>
        </probe>
        <probe name="TeamCity">
            <hostname>teamcity.training.local</hostname>
            <port>7036</port>
        </probe>
        <probe name="ActiveMQ">
            <hostname>activemq.training.local</hostname>
            <port>7036</port>
        </probe>
        <probe name="TeamCityAgent1">
            <hostname>172.31.32.204</hostname>
            <port>7036</port>
        </probe>
        <probe name="MySQL and Mongo">
            <hostname>mysql.training.local</hostname>
            <port>55901</port>
        </probe>
        <probe name="TeamCity Agent 2">
            <hostname>172.31.35.200</hostname>
            <port>7036</port>
        </probe>
        <probe disabled="true" name="Oracle">
            <hostname>oracle.training.local</hostname>
            <port>7036</port>
        </probe>
        <probe name="Ansible">
            <hostname>172.31.22.122</hostname>
            <port>7306</port>
        </probe>
    </probes>
    <samplers>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    </samplers>
    <actions>
        <action name="Create my file">
            <script>
                <exeFile>/usr/bin/touch /tmp/steve</exeFile>
                <arguments>
                    <data></data>
                </arguments>
                <runLocation>netprobe</runLocation>
                <probe ref="ITRS"></probe>
            </script>
        </action>
        <action name="SSHRestart">
            <script>
                <exeFile>/sbin/service sshd restart</exeFile>
                <arguments>
                    <data></data>
                </arguments>
                <runLocation>netprobe</runLocation>
            </script>
        </action>
    </actions>
    <commands>
        <command name="Restart SSH">
            <userCommand>
                <type>script</type>
                <runLocation>netprobe</runLocation>
                <args>
                    <arg>
                        <static>/sbin/service sshd restart</static>
                    </arg>
                </args>
                <secureEnvironmentVariables>
                    <variable>
                        <name>PASS</name>
                        <value>
                            <stdAES>+encs+88E278F1FEAC29763655C7925C24B8DD</stdAES>
                        </value>
                    </variable>
                </secureEnvironmentVariables>
            </userCommand>
        </command>
    </commands>
    <activeTimes>
        <activeTime name="Business Hours UK">
            <scheduledPeriod>
                <startTime>08:00:00</startTime>
                <endTime>18:00:00</endTime>
                <days>
                    <monday>true</monday>
                    <tuesday>true</tuesday>
                    <wednesday>true</wednesday>
                    <thursday>true</thursday>
                    <friday>true</friday>
                    <saturday>false</saturday>
                    <sunday>false</sunday>
                </days>
            </scheduledPeriod>
        </activeTime>
    </activeTimes>
    <environments>
        <environmentGroup name="EMEA">
            <environment name="EMEADEV">
                <var name="IntervalTime">
                    <integer>10</integer>
                </var>
                <var name="DBPass">
                    <stdEncodedPassword>
                        <std>+en+tddqfs</std>
                    </stdEncodedPassword>
                </var>
            </environment>
            <environment name="EMEAPROD">
                <var name="IntervalTime">
                    <integer>30</integer>
                </var>
                <var name="DBPass">
                    <stdEncodedPassword>
                        <std>+en+tddqfs</std>
                    </stdEncodedPassword>
                </var>
            </environment>
        </environmentGroup>
        <environmentGroup name="NAM">
            <environment name="NAMDEV">
                <var name="IntervalTime">
                    <integer>5</integer>
                </var>
                <var name="DBPass">
                    <stdEncodedPassword>
                        <std>+en+tddqfs</std>
                    </stdEncodedPassword>
                </var>
            </environment>
            <environment name="NAMPROD">
                <var name="IntervalTime">
                    <integer>15</integer>
                </var>
                <var name="DBPass">
                    <stdEncodedPassword>
                        <std>+en+tddqfs</std>
                    </stdEncodedPassword>
                </var>
            </environment>
        </environmentGroup>
    </environments>
    <knowledgeBase>
        <urlTemplate name="Martins Notes">
            <urlTemplateElements>
                <urlTemplateElement>
                    <literal>http://citi.oldfrog.com</literal>
                </urlTemplateElement>
            </urlTemplateElements>
        </urlTemplate>
        <kbaSet name="PS_Wiki_Conygre">
            <kba>
                <label>PS_Wiki_Conygre</label>
                <urlElements>
                    <urlElement>
                        <literal>http://ansible.conygre.com/wiki/doku.php?id=start</literal>
                    </urlElement>
                </urlElements>
                <targets>
                    <target>/geneos/gateway/directory/probe/managedEntity/sampler/dataview/rows/row/cell</target>
                </targets>
            </kba>
        </kbaSet>
        <kbaSet name="Google">
            <kba>
                <label>Google</label>
                <urlElements>
                    <urlElement>
                        <literal>https://www.google.com/</literal>
                    </urlElement>
                </urlElements>
                <targets>
                    <target>/geneos/gateway/directory/probe/managedEntity/sampler/dataview/rows/row/cell</target>
                </targets>
            </kba>
        </kbaSet>
    </knowledgeBase>
    <operatingEnvironment>
        <!--The gateway name must be set, the listen port should be set-->
        <gatewayName>DC</gatewayName>
        <listenPort>55809</listenPort>
    </operatingEnvironment>
</gateway>