#!/bin/bash
# Bash script shamelessly stolen from by Saad Ismail but altered !!

# If /root/.my.cnf exists then it won't ask for root password
if [ -f /root/.my.cnf ]; then
	echo "Creating new database..."
	mysql -e "CREATE DATABASE test;"
	echo "Database successfully created!"
	echo "Showing existing databases..."
	mysql -e "show databases;"
	echo ""
	echo "Please enter the NAME of the new database user! (example: user1)"
	read username
	echo "Please enter the PASSWORD for the new database user!"
	read userpass
	echo "Creating new user..."
	mysql -e "CREATE USER ${username}@localhost IDENTIFIED BY '${userpass}';"
	echo "User successfully created!"
	echo ""
	echo "Granting ALL privileges on ${dbname} to ${username}!"
	mysql -e "GRANT ALL PRIVILEGES ON ${dbname}.* TO '${username}'@'localhost';"
	mysql -e "FLUSH PRIVILEGES;"
	echo "You're good now :)"
	exit
	
# If /root/.my.cnf doesn't exist then it'll ask for root password	
else
	echo "Please enter root user MySQL password!"
	read rootpasswd
	echo "Please enter the NAME of the new database! (example: database1)"
	read dbname
	echo "Creating new database..."
	mysql -u root -p${rootpasswd} -e "CREATE DATABASE ${dbname}"
	echo "Database successfully created!"
	echo "Showing existing databases..."
	mysql -u root -p${rootpasswd} -e "show databases;"
	echo ""
	echo "Please enter the NAME of the new database user! (example: user1)"
	read username
	echo "Please enter the PASSWORD for the new database user!"
	read userpass
	echo "Creating new user..."
	mysql -u root -p${rootpasswd} -e "CREATE USER ${username}@localhost IDENTIFIED BY '${userpass}';"
	echo "User successfully created!"
	echo ""
	echo "Granting ALL privileges on ${dbname} to ${username}!"
	mysql -u root -p${rootpasswd} -e "GRANT ALL PRIVILEGES ON ${dbname}.* TO '${username}'@'localhost';"
	mysql -u root -p${rootpasswd} -e "FLUSH PRIVILEGES;"
	echo "You're good now :)"
	exit
fi
