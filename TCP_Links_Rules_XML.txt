<gateway compatibility="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://schema.itrsgroup.com/GA4.1.0-170114/gateway.xsd">
    <rules>
        <rule name="ConnectionCheckTCP">
            <targets>
                <target>/geneos/gateway/directory/probe[(@name=&quot;uDeploy&quot;)]/managedEntity[(@name=&quot;uDeploy&quot;)]/sampler[(@name=&quot;tcp-links-udeploy&quot;)][(@type=&quot;uDeploy&quot;)]/dataview[(@name=&quot;tcp-links-udeploy&quot;)]/headlines/cell[(@name=&quot;Total connections&quot;)]</target>
                <target>/geneos/gateway/directory/probe[(@name=&quot;TeamCity&quot;)]/managedEntity[(@name=&quot;TeamCity Server&quot;)]/sampler[(@name=&quot;tcp-links-teamcity&quot;)][(@type=&quot;TeamCity - Server&quot;)]/dataview[(@name=&quot;tcp-links-teamcity&quot;)]/headlines/cell[(@name=&quot;Total connections&quot;)]</target>
                <target>/geneos/gateway/directory/probe[(@name=&quot;TestAppServer&quot;)]/managedEntity[(@name=&quot;Test Server&quot;)]/sampler[(@name=&quot;tcp-links-testappserver&quot;)][(@type=&quot;TestAppServer&quot;)]/dataview[(@name=&quot;tcp-links-testappserver&quot;)]/headlines/cell[(@name=&quot;Total connections&quot;)]</target>
                <target>/geneos/gateway/directory/probe[(@name=&quot;ProdAppServer&quot;)]/managedEntity[(@name=&quot;App Server&quot;)]/sampler[(@name=&quot;tcp-links-prodappserver&quot;)][(@type=&quot;ProdAppServer&quot;)]/dataview[(@name=&quot;tcp-links-prodappserver&quot;)]/headlines/cell[(@name=&quot;Total connections&quot;)]</target>
                <target>/geneos/gateway/directory/probe[(@name=&quot;MySQL and Mongo&quot;)]/managedEntity[(@name=&quot;MySQL and Mongo&quot;)]/sampler[(@name=&quot;tcp-links-mysql&quot;)][(@type=&quot;DB - MySQL&quot;)]/dataview[(@name=&quot;tcp-links-mysql&quot;)]/headlines/cell[(@name=&quot;Total connections&quot;)]</target>
                <target>/geneos/gateway/directory/probe[(@name=&quot;MySQL and Mongo&quot;)]/managedEntity[(@name=&quot;MySQL and Mongo&quot;)]/sampler[(@name=&quot;tcp-links-mongodb&quot;)][(@type=&quot;DB - Mongo&quot;)]/dataview[(@name=&quot;tcp-links-mongodb&quot;)]/headlines/cell[(@name=&quot;Total connections&quot;)]</target>
                <target>/geneos/gateway/directory/probe[(@name=&quot;ActiveMQ&quot;)]/managedEntity[(@name=&quot;ActiveMQ&quot;)]/sampler[(@name=&quot;tcp-links-activemq&quot;)][(@type=&quot;ActiveMQ&quot;)]/dataview[(@name=&quot;tcp-links-activemq&quot;)]/headlines/cell[(@name=&quot;Total connections&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;TeamCity&quot;)]/managedEntity[(@name=&quot;TeamCity Server&quot;)]/sampler[(@name=&quot;tcp-links-Bitbucket&quot;)][(@type=&quot;TeamCity - Server&quot;)]/dataview[(@name=&quot;tcp-links-Bitbucket&quot;)]/headlines/cell[(@name=&quot;Total connections&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;Docker&quot;)]/managedEntity[(@name=&quot;Docker Registry&quot;)]/sampler[(@name=&quot;tcp-links-docker&quot;)][(@type=&quot;Docker Connect&quot;)]/dataview[(@name=&quot;tcp-links-docker&quot;)]/headlines/cell[(@name=&quot;Total connections&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;ELK&quot;)]/managedEntity[(@name=&quot;ELK&quot;)]/sampler[(@name=&quot;tcp-links-ELK&quot;)][(@type=&quot;ELK&quot;)]/dataview[(@name=&quot;tcp-links-ELK&quot;)]/headlines/cell[(@name=&quot;Total connections&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;ITRS&quot;)]/managedEntity[(@name=&quot;ITRS&quot;)]/sampler[(@name=&quot;tcp-links-ITRS&quot;)][(@type=&quot;ITRS&quot;)]/dataview[(@name=&quot;tcp-links-ITRS&quot;)]/headlines/cell[(@name=&quot;Total connections&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;TeamCity Agent 2&quot;)]/managedEntity[(@name=&quot;TeamCity Agent 2&quot;)]/sampler[(@name=&quot;tcp-links-Bitbucket&quot;)][(@type=&quot;TCAgent&quot;)]/dataview[(@name=&quot;tcp-links-Bitbucket&quot;)]/headlines/cell[(@name=&quot;Total connections&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;uDeploy&quot;)]/managedEntity[(@name=&quot;uDeploy&quot;)]/sampler[(@name=&quot;tcp-links-openshift&quot;)][(@type=&quot;uDeploy&quot;)]/dataview[(@name=&quot;tcp-links-openshift&quot;)]/headlines/cell[(@name=&quot;Total connections&quot;)]</target>
                <target>/geneos/gateway/directory/probe/managedEntity/sampler[(@name=&quot;tcp-links-MarketFeed&quot;)][(@type=&quot;ProdAppServer&quot;)]/dataview[(@name=&quot;tcp-links-MarketFeed&quot;)]/headlines/cell[(@name=&quot;Total connections&quot;)]</target>
                <target>/geneos/gateway/directory/probe/managedEntity/sampler[(@name=&quot;tcp-links-MarketFeed&quot;)][(@type=&quot;TestAppServer&quot;)]/dataview[(@name=&quot;tcp-links-MarketFeed&quot;)]/headlines/cell[(@name=&quot;Total connections&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;uDeploy&quot;)]/managedEntity[(@name=&quot;uDeploy&quot;)]/sampler[(@name=&quot;tcp-links-openshift 1&quot;)][(@type=&quot;uDeploy&quot;)]/dataview[(@name=&quot;tcp-links-openshift 1&quot;)]/headlines/cell[(@name=&quot;Total connections&quot;)]</target>
            </targets>
            <priority>1</priority>
            <block>
                <if>
                    <gt>
                        <dataItem>
                            <property>@value</property>
                        </dataItem>
                        <integer>0</integer>
                    </gt>
                    <transaction>
                        <update>
                            <property>state/@severity</property>
                            <severity>ok</severity>
                        </update>
                    </transaction>
                    <transaction>
                        <update>
                            <property>state/@severity</property>
                            <severity>critical</severity>
                        </update>
                    </transaction>
                </if>
            </block>
        </rule>
        <rule name="OtherTCPChecks">
            <targets>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;Oracle&quot;)]/managedEntity[(@name=&quot;Oracle&quot;)]/sampler[(@name=&quot;tcp-links-oracle&quot;)][(@type=&quot;DB - Oracle&quot;)]/dataview[(@name=&quot;tcp-links-oracle&quot;)]/rows/row/cell[(@column=&quot;state&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;ELK&quot;)]/managedEntity[(@name=&quot;ELK&quot;)]/sampler[(@name=&quot;tcp-links-ELK&quot;)][(@type=&quot;ELK&quot;)]/dataview[(@name=&quot;tcp-links-ELK&quot;)]/rows/row/cell[(@column=&quot;state&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;ITRS&quot;)]/managedEntity[(@name=&quot;ITRS&quot;)]/sampler[(@name=&quot;tcp-links-ITRS&quot;)][(@type=&quot;ITRS&quot;)]/dataview[(@name=&quot;tcp-links-ITRS&quot;)]/rows/row/cell[(@column=&quot;state&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;TeamCity&quot;)]/managedEntity[(@name=&quot;TeamCity Server&quot;)]/sampler[(@name=&quot;tcp-links-Bitbucket&quot;)][(@type=&quot;TeamCity - Server&quot;)]/dataview[(@name=&quot;tcp-links-Bitbucket&quot;)]/rows/row/cell[(@column=&quot;state&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;Docker&quot;)]/managedEntity[(@name=&quot;Docker Registry&quot;)]/sampler[(@name=&quot;tcp-links-docker&quot;)][(@type=&quot;Docker Connect&quot;)]/dataview[(@name=&quot;tcp-links-docker&quot;)]/rows/row/cell[(@column=&quot;state&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;TestAppServer&quot;)]/managedEntity[(@name=&quot;Test Server&quot;)]/sampler[(@name=&quot;tcp-links-testappserver&quot;)][(@type=&quot;TestAppServer&quot;)]/dataview[(@name=&quot;tcp-links-testappserver&quot;)]/rows/row/cell[(@column=&quot;state&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;TeamCity&quot;)]/managedEntity[(@name=&quot;TeamCity Server&quot;)]/sampler[(@name=&quot;tcp-links-teamcity&quot;)][(@type=&quot;TeamCity - Server&quot;)]/dataview[(@name=&quot;tcp-links-teamcity&quot;)]/rows/row/cell[(@column=&quot;state&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;ProdAppServer&quot;)]/managedEntity[(@name=&quot;App Server&quot;)]/sampler[(@name=&quot;tcp-links-prodappserver&quot;)][(@type=&quot;ProdAppServer&quot;)]/dataview[(@name=&quot;tcp-links-prodappserver&quot;)]/rows/row/cell[(@column=&quot;state&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;MySQL and Mongo&quot;)]/managedEntity[(@name=&quot;MySQL and Mongo&quot;)]/sampler[(@name=&quot;tcp-links-mysql&quot;)][(@type=&quot;DB - MySQL&quot;)]/dataview[(@name=&quot;tcp-links-mysql&quot;)]/rows/row/cell[(@column=&quot;state&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;MySQL and Mongo&quot;)]/managedEntity[(@name=&quot;MySQL and Mongo&quot;)]/sampler[(@name=&quot;tcp-links-mongodb&quot;)][(@type=&quot;DB - Mongo&quot;)]/dataview[(@name=&quot;tcp-links-mongodb&quot;)]/rows/row/cell[(@column=&quot;state&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;ActiveMQ&quot;)]/managedEntity[(@name=&quot;ActiveMQ&quot;)]/sampler[(@name=&quot;tcp-links-activemq&quot;)][(@type=&quot;ActiveMQ&quot;)]/dataview[(@name=&quot;tcp-links-activemq&quot;)]/rows/row/cell[(@column=&quot;state&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;uDeploy&quot;)]/managedEntity[(@name=&quot;uDeploy&quot;)]/sampler[(@name=&quot;tcp-links-udeploy&quot;)][(@type=&quot;uDeploy&quot;)]/dataview[(@name=&quot;tcp-links-udeploy&quot;)]/rows/row/cell[(@column=&quot;state&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;uDeploy&quot;)]/managedEntity[(@name=&quot;uDeploy&quot;)]/sampler[(@name=&quot;tcp-links-openshift&quot;)][(@type=&quot;uDeploy&quot;)]/dataview[(@name=&quot;tcp-links-openshift&quot;)]/rows/row/cell[(@column=&quot;state&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;TeamCityAgent1&quot;)]/managedEntity[(@name=&quot;TeamCity Agent 1&quot;)]/sampler[(@name=&quot;tcp-links-Bitbucket&quot;)][(@type=&quot;TCAgent&quot;)]/dataview[(@name=&quot;tcp-links-Bitbucket&quot;)]/rows/row/cell[(@column=&quot;state&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;TeamCity Agent 2&quot;)]/managedEntity[(@name=&quot;TeamCity Agent 2&quot;)]/sampler[(@name=&quot;tcp-links-Bitbucket&quot;)][(@type=&quot;TCAgent&quot;)]/dataview[(@name=&quot;tcp-links-Bitbucket&quot;)]/rows/row/cell[(@column=&quot;state&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;ProdAppServer&quot;)]/managedEntity[(@name=&quot;App Server&quot;)]/sampler[(@name=&quot;tcp-links-MarketFeed&quot;)][(@type=&quot;ProdAppServer&quot;)]/dataview[(@name=&quot;tcp-links-MarketFeed&quot;)]/rows/row/cell[(@column=&quot;state&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;TestAppServer&quot;)]/managedEntity[(@name=&quot;Test Server&quot;)]/sampler[(@name=&quot;tcp-links-MarketFeed&quot;)][(@type=&quot;TestAppServer&quot;)]/dataview[(@name=&quot;tcp-links-MarketFeed&quot;)]/rows/row/cell[(@column=&quot;state&quot;)]</target>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;uDeploy&quot;)]/managedEntity[(@name=&quot;uDeploy&quot;)]/sampler[(@name=&quot;tcp-links-openshift 1&quot;)][(@type=&quot;uDeploy&quot;)]/dataview[(@name=&quot;tcp-links-openshift 1&quot;)]/rows/row/cell[(@column=&quot;state&quot;)]</target>
            </targets>
            <priority>2</priority>
            <block>
                <if>
                    <or>
                        <or>
                            <equal>
                                <dataItem>
                                    <property>@value</property>
                                </dataItem>
                                <string>LISTEN</string>
                            </equal>
                            <equal>
                                <dataItem>
                                    <property>@value</property>
                                </dataItem>
                                <string>ESTABLISHED</string>
                            </equal>
                        </or>
                        <equal>
                            <dataItem>
                                <property>@value</property>
                            </dataItem>
                            <string>TIME_WAIT</string>
                        </equal>
                    </or>
                    <transaction>
                        <update>
                            <property>state/@severity</property>
                            <severity>ok</severity>
                        </update>
                    </transaction>
                    <transaction>
                        <update>
                            <property>state/@severity</property>
                            <severity>critical</severity>
                        </update>
                    </transaction>
                </if>
                <if>
                    <equal>
                        <equal>
                            <count>
                                <dataItem>
                                    <property>@value</property>
                                </dataItem>
                            </count>
                            <gt>
                                <string>TIME_WAIT</string>
                                <count>
                                    <dataItem>
                                        <property>@value</property>
                                    </dataItem>
                                </count>
                            </gt>
                        </equal>
                        <string>ESTABLISHED</string>
                    </equal>
                    <transaction>
                        <update>
                            <property>state/@severity</property>
                            <severity>warning</severity>
                        </update>
                    </transaction>
                </if>
            </block>
        </rule>
        <rule name="TotalConnectionsMySQL">
            <targets>
                <target>/geneos/gateway[(@name=&quot;DC&quot;)]/directory/probe[(@name=&quot;MySQL and Mongo&quot;)]/managedEntity[(@name=&quot;MySQL and Mongo&quot;)]/sampler[(@name=&quot;tcp-links-mysql&quot;)][(@type=&quot;DB - MySQL&quot;)]/dataview[(@name=&quot;tcp-links-mysql&quot;)]/headlines/cell[(@name=&quot;Total connections&quot;)]</target>
            </targets>
            <priority>1</priority>
            <block>
                <if>
                    <or>
                        <gt>
                            <dataItem>
                                <property>@value</property>
                            </dataItem>
                            <integer>500</integer>
                        </gt>
                        <equal>
                            <dataItem>
                                <property>@value</property>
                            </dataItem>
                            <integer>0</integer>
                        </equal>
                    </or>
                    <transaction>
                        <update>
                            <property>state/@severity</property>
                            <severity>critical</severity>
                        </update>
                    </transaction>
                    <if>
                        <gt>
                            <dataItem>
                                <property>@value</property>
                            </dataItem>
                            <integer>400</integer>
                        </gt>
                        <transaction>
                            <update>
                                <property>state/@severity</property>
                                <severity>warning</severity>
                            </update>
                        </transaction>
                        <transaction>
                            <update>
                                <property>state/@severity</property>
                                <severity>ok</severity>
                            </update>
                        </transaction>
                    </if>
                </if>
            </block>
        </rule>
    </rules>
</gateway>