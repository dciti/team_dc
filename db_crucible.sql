SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema initdb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema initdb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `initdb` DEFAULT CHARACTER SET utf8 ;
USE `initdb` ;

-- -----------------------------------------------------
-- Table `initdb`.`strategy`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `initdb`.`strategy` (
 `StrategyID` INT(11) NOT NULL AUTO_INCREMENT,
 `startingQuantity` INT(11) NOT NULL,
 `budget` DOUBLE NOT NULL,
 `startDate` DATETIME NOT NULL,
 `endDate` DATETIME NOT NULL,
 `stock` VARCHAR(5) NOT NULL,
 `standardDeviationSell` INT NOT NULL,
 `standardDeviationBuy` INT NOT NULL,
 PRIMARY KEY (`StrategyID`),
 UNIQUE INDEX `StrategyID_UNIQUE` (`StrategyID` ASC),
 INDEX `TradeID_idx` (`EndDate` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `initdb`.`trades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `initdb`.`trades` (
 `id` INT(11) NOT NULL AUTO_INCREMENT,
 `tradeType` VARCHAR(5) NOT NULL,
 `stock` VARCHAR(12) NOT NULL,
 `tradedQuantity` INT(11) NOT NULL,
 `lastStateChange` DATETIME(3) NOT NULL,
 `state` VARCHAR(20) NOT NULL,
 `StrategyID` INT NOT NULL,
 PRIMARY KEY (`id`),
 INDEX `StrategyId_idx` (`StrategyID` ASC),
 CONSTRAINT `StrategyId`
   FOREIGN KEY (`StrategyID`)
   REFERENCES `initdb`.`strategy` (`StrategyID`)
   ON DELETE NO ACTION
   ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;